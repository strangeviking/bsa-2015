// Man
var Man = function(name, age) {    
    this.name = name;
    this.age = age;
}

Man.prototype.live = function() {
    return "I'm living.";
};

// Student
var Student = function(name, age) {    
    Man.call(this, name, age);
};

Student.prototype = new Man();

Student.prototype.study = function() {
    return "I'm studying";
};

// Professor
var Professor = function(name, age, subject) {    
    Man.call(this, name, age);
    this.subject = subject;
};

Professor.prototype = new Man();

Professor.prototype.teach = function() {
    return "I'm teaching.";
};