// Man spec
    describe("Man", function() {
        it("call the live() method", function() {
            var fakeMan = new Man("Henry", 37);
            spyOn(fakeMan, "live");
            fakeMan.live();
            expect(fakeMan.live).toHaveBeenCalled();
        });

        it("method live() should return response", function() {
            var fakeMan = new Man("Henry", 37);            
            var response = fakeMan.live();
            expect(response).toEqual("I'm living.");
        });

        it("Man has correct name and age", function() {
            var fakeMan = new Man("Henry", 37);            
            expect(fakeMan.name).toEqual("Henry");
            expect(fakeMan.age).toEqual(37);
        });
    });
// Student spec
    describe("Student", function() {        
        it("call the study() method", function() {
            var fakeStudent = new Student("Jesse", 18);
            spyOn(fakeStudent, "study");
            fakeStudent.study();
            expect(fakeStudent.study).toHaveBeenCalled();
        });

        it("method study() should return response", function() {
            var fakeStudent = new Student("Jesse", 18);
            var response = fakeStudent.study();
            expect(response).toEqual("I'm studying");
        });

        it("Student has correct name and age", function() {
            var fakeStudent = new Student("Jesse", 18);
            expect(fakeStudent.name).toEqual("Jesse");
            expect(fakeStudent.age).toEqual(18);
        });

        it("Student inherits a Man's live() method", function() {
            var fakeStudent = new Student("Jesse", 18);
            var response = fakeStudent.live();
            expect(response).toEqual("I'm living.");
        });

        it("Student do not have die() method", function() {
            var fakeStudent = new Student("Jesse", 18);
            expect( function(){ fakeStudent.die(); } ).toThrow(new TypeError("fakeStudent.die is not a function"));            
        });
    });

// Professor spec
    describe("Professor", function() {        
        it("call the teach() method", function() {
            var fakeProfessor = new Professor("Walter", 52, "Chemistry");
            spyOn(fakeProfessor, "teach");
            fakeProfessor.teach();
            expect(fakeProfessor.teach).toHaveBeenCalled();
        });

        it("method teach() should return response", function() {
            var fakeProfessor = new Professor("Walter", 52, "Chemistry");
            var response = fakeProfessor.teach();
            expect(response).toEqual("I'm teaching.");
        });

        it("Professor has correct name, age and subject", function() {
            var fakeProfessor = new Professor("Walter", 52, "Chemistry");
            expect(fakeProfessor.name).toEqual("Walter");
            expect(fakeProfessor.age).toEqual(52);
            expect(fakeProfessor.subject).toEqual("Chemistry");
        });

        it("Professor inherits a Man's live() method", function() {
            var fakeProfessor = new Professor("Walter", 52, "Chemistry");
            var response = fakeProfessor.live();
            expect(response).toEqual("I'm living.");
        });

        it("Professor do not have making_drugs() method", function() {
            var fakeProfessor = new Professor("Walter", 52, "Chemistry");
            expect( function(){ fakeProfessor.making_drugs(); } ).toThrow(new TypeError("fakeProfessor.making_drugs is not a function"));            
        });
    });
