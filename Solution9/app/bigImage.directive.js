    angular
        .module("app")
        .directive("bigImage", bigImage);

    function bigImage() {
        return {
            restrict: "A",
            link: link
        };

        function link(scope, element, attributes) {
            var imagePath = "";

            attributes.$observe("bigImage", function(value) {
                imagePath = value;
            });

            element.bind("click", elementClick);

            function elementClick() {
                
                var background = document.createElement("div");

                
                var backgroundStyle = document.createAttribute("style");
                backgroundStyle.value = "position:fixed;" +
                    "height:100%;width:100%;" +
                    "background:rgba(0,0,0,0.8);" +
                    "top:50%;left:50%;transform:translate(-50%,-50%);";
                background.setAttributeNode(backgroundStyle);

                var image = document.createElement("img");

                var imageSrc = document.createAttribute("src");
                imageSrc.value = imagePath;
                image.setAttributeNode(imageSrc);

                var imageStyle = document.createAttribute("style");
                imageStyle.value = "position:fixed;" +
                    "top:50%;left:50%;transform:translate(-50%,-50%);";
                image.setAttributeNode(imageStyle);

                background.appendChild(image);
                document.body.appendChild(background);

                background.addEventListener("click", function() {
                    document.body.removeChild(background);
                });
            }
        }
    }