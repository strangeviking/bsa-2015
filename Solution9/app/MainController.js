    angular
        .module("app")
        .controller("MainController", MainController);

    MainController.$inject = ["httpFactory", "Photos"];

    function MainController(httpFactory, Photos) {
        var vm = this;
        vm.photos = [];

        load();

        function load() {
            return httpFactory.getPhotosData().then(function(result) {
                vm.photos = result;
                return vm.photos;
            });
        }
    }