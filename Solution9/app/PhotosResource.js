    angular
        .module("app")
        .factory("Photos", Photos);

    Photos.$inject = ["$q", "$resource"];

    function Photos($q, $resource) {
        return {
            getPhotosData: getPhotosData
        };

        function getPhotosData() {
            var deferred = $q.defer();

            var Photos = $resource("http://jsonplaceholder.typicode.com/photos:id",
                {
                    id: "@albumId"
                }
            );

            Photos.query({albumId: 0}, function(response) {
                deferred.resolve(response);
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }
    }