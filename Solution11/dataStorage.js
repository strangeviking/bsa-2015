var countries = [
  { name : 'Ukraine', description : "UA"},
  { name : 'United Kingdom', description : "UK"},
  { name : 'Germany', description : "DE"},
  { name : 'Poland', description : "PL"}
];

var hotels = [
  { ID : '0', name: 'Hilton', country: 'United Kingdom', description : 'UK 1' },
  { ID : '1', name: 'Rudding Park Hotel', country: 'United Kingdom', description : 'UK 2' },
  { ID : '2', name: 'The Milestone Hotel', country: 'United Kingdom', description : 'UK 3' },
  { ID : '3', name: '11 Mirrors Design', country: 'Ukraine', description : 'UA 1' },
  { ID : '4', name: 'Frederic Koklen', country: 'Ukraine', description : 'UA 2' },
  { ID : '5', name: 'Kharkiv Palace', country: 'Ukraine', description : 'UK 3' },
  { ID : '6', name: 'Herrnschloesschen', country: 'Germany', description : 'DE 1' },
  { ID : '7', name: 'Kurpfalzhof', country: 'Germany', description : 'DE 2' },
  { ID : '8', name: 'Gablerhof', country: 'Germany', description : 'DE 3' },
  { ID : '9', name: 'Metropolitan', country: 'Poland', description : 'PL 1' },
  { ID : '10', name: 'Topolowa', country: 'Poland', description : 'PL 2' },
  { ID : '11', name: 'The Blue Beetroot', country: 'Poland', description : 'PL 3' }
];

exports.listCountries = function () {
	return countries;
};
exports.getHotelsInCountry = function (countryName) {
        var selectedHotels = [];
        for (hotel in hotels) {
            if (hotels[hotel].country == countryName) selectedHotels.push(hotels[hotel]);
        }
        return selectedHotels;
    };
exports.addCountry = function (countryName, countryDescription) {
	countries.push({name: countryName, description: countryDescription});
};
exports.addHotel = function (countryName, hotelName, hotelDescription) {
        var hotelId = hotels.length + 1;
        hotels.push({id: hotelId, country: countryName, name: hotelName, description: hotelDescription});
    };
exports.deleteHotel = function (id) {
        for (hotel in hotels) {
            if (hotels[hotel].id == id) {
                hotels.splice(hotel, 1);
                break;
            }
        }
};
exports.updateHotel = function (id, countryName, hotelName, hotelDescription) {
        for (hotel in hotels) {
            if (hotels[hotel].id == id) {
                hotels[hotel].country = countryName;
                hotels[hotel].name = hotelName;
                hotels[hotel].description = hotelDescription;
                break;
            }
        }
    };