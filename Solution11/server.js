var http         = require('http');
var dataStorage = require('./dataStorage.js');
var Router       = require('router');
var bodyParser   = require('body-parser');
var finalhandler = require('finalhandler');


var router = Router();
router.use(bodyParser.urlencoded({ extended: true }));

router.get('/countries', function (req, res) {	
	res.end(JSON.stringify({
		"status": "success",
		"countries": dataStorage.listCountries()
	}));
});
 
router.get('/country/:country/hotels', function (req, res) {
	var name = req.params.country;
	res.end(JSON.stringify({
		"status": "success",
		"hotels": dataStorage.getHotelsInCountry(name)
	}));
});

router.post('/country', function (req, res) {	
	var name = req.body.countryName;
	var description = req.body.countryDescription;
	dataStorage.addCountry(name, description);
	res.end(JSON.stringify({
		"status": "success"
	}));
});

router.post('/country/:name/hotel', function (req, res) {	
	var countryName = req.params.name;	
	var hotelName = req.body.hotelName;
	var hotelDescription = req.body.hotelDescription;
	dataStorage.addHotel(countryName, hotelName, hotelDescription);	
	res.end(JSON.stringify({
		"status": "success"
	}));
});

router.delete('/hotel/:id', function (req, res) {	
	var id = parseInt(req.params.id);		
	dataStorage.deleteHotel(id);
	res.end(JSON.stringify({
		"status": "success"
	}));
});

router.put('/hotel/:id', function (req, res) {
	var id = parseInt(req.params.id);
	var newCountryName = req.body.countryName;
	var newHotelName = req.body.hotelName;
	var newHotelDescription = req.body.hotelDescription;
	dataStorage.updateHotel(id, newCountryName, newHotelName, newHotelDescription);
	res.end(JSON.stringify({
		"status": "success"
	}));
});

var server = http.createServer(function(req, res) {
	res.setHeader('Content-Type', 'text/plain; charset=utf-8');
	router(req, res, finalhandler(req, res));
});
console.log("Server started at http://127.0.0.1:8080/");
server.listen(8080);