var express = require('express');
var bodyParser = require('body-parser');
var db = require('./dataStorage.js');

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));

var router = express.Router();
router.use(bodyParser.urlencoded({ extended: true }));

router.get('/country', function (req, res) {	
	res.end(JSON.stringify({
		"countries": dataStorage.listCountries()
	}));
});
 
router.get('/country/:country/hotel', function (req, res) {
	var name = req.params.country;
	res.end(JSON.stringify({
		"hotels": dataStorage.getHotelsInCountry(name)
	}));
});

router.post('/country', function (req, res) {	
	var name = req.body.countryName;
	var description = req.body.countryDescription;
	dataStorage.addCountry(name, description);
});

router.post('/country/:name/hotel', function (req, res) {	
	var countryName = req.params.name;	
	var hotelName = req.body.hotelName;
	var hotelDescription = req.body.hotelDescription;	
	dataStorage.addHotel(countryName, hotelName, hotelDescription);
});

router.delete('/hotel/:id', function (req, res) {	
	var id = parseInt(req.params.id);	
	dataStorage.deleteHotel(id);
});

router.put('/hotel/:id', function (req, res) {
	var id = parseInt(req.params.id);
	var newCountryName = req.body.countryName;
	var newHotelName = req.body.hotelName;
	var newHotelDescription = req.body.hotelDescription;	
	dataStorage.updateHotel(id, newCountryName, newHotelName, newHotelDescription);
});
app.use('/', router);
app.listen(8080);

console.log("Server started at http://127.0.0.1:8080/");
